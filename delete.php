<?php
require_once "inc/top.php";
?>

<?php
try {
    $idt = filter_input(INPUT_POST, "id", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    $where = 0;
    if ($idt) {
        foreach ($idt as $id) {
            if (is_numeric($id)) {
                if (strlen($where) > 0) {
                    $where .= " OR ";
                }
                $where .= "id = $id";
            }            
        }
    }

    if (strlen($where) > 0) {
        $sql = "delete from task where $where;";
        $query = $db->query($sql);
        $query->execute();
    }
    print "<p>Valitut tehtävät on poistettu listalta!</p>";
} catch (PDOException $pdoex) {
    print "<p>Tietojen päivittäminen epäonnistui. " . $pdoex->getMessage() . "</p>";
}
?>
<a href="index.php">Tehtävälistaan</a>

<?php
require_once "inc/bottom.php";
?>